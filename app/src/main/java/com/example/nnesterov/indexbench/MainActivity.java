package com.example.nnesterov.indexbench;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends ActionBarActivity {

    private static final int DATA_COUNT = 1000000;
    private static final int REPEAT_COUNT = 5;
    private static final int MAX_VALUE = 100;
    private static final int HALF_VALUE = MAX_VALUE / 2;

    private static final int INDEX_BASED_ARRAY_LIST = 0;
    private static final int FOREACH_ARRAY_LIST = 1;
    private static final int INDEX_BASED_ARRAY = 2;
    private static final int FOREACH_ARRAY = 3;

    @InjectView(R.id.button_run)
    Button buttonRun;

    @InjectView(R.id.progress_indicator)
    ProgressBar progressIndicator;

    @InjectView(R.id.result_textview)
    TextView resultView;

    private final Random random = new Random(System.currentTimeMillis());


    private ArrayList<Integer> arrayList = new ArrayList<>();
    private int[] array = new int[DATA_COUNT];

    /**
     * 0 - index-based ArrayList
     * 1 - foreach ArrayList
     * 2 - index-based LinkedList
     * 3 - foreach LinkedList
     */
    private final List<List<Long>> results = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        buttonRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setUp() {
        results.clear();
        for (int i = 0; i < 4; ++i) {
            results.add(new ArrayList<Long>());
        }
    }

    private void generateData() {
        arrayList = new ArrayList<>(DATA_COUNT);
        array  = new int[DATA_COUNT];

        for (int i = 0; i < DATA_COUNT; ++i) {
            arrayList.add(random.nextInt(MAX_VALUE));
        }

        for (int i = 0; i < DATA_COUNT; ++i) {
            array[i] =random.nextInt(MAX_VALUE);
        }
    }

    private void start() {
        setInProgress(true);
        setUp();

        for (List<Long> result : results) {
            result.clear();
        }

        performBench();
        processResults();
        displayResults();

        setInProgress(false);

    }

    private void performBench() {
        for (int i = 0; i < REPEAT_COUNT; ++i) {
            generateData();

            results.get(FOREACH_ARRAY_LIST).add(benchForeachArrayList());
            results.get(INDEX_BASED_ARRAY_LIST).add(benchIndexArrayList());

            results.get(FOREACH_ARRAY).add(benchForeachArray());
            results.get(INDEX_BASED_ARRAY).add(benchIndexArray());
        }

        for (int i = 0; i < REPEAT_COUNT; ++i) {
            generateData();

            results.get(INDEX_BASED_ARRAY_LIST).add(benchIndexArrayList());
            results.get(FOREACH_ARRAY_LIST).add(benchForeachArrayList());

            results.get(INDEX_BASED_ARRAY).add(benchIndexArray());
            results.get(FOREACH_ARRAY).add(benchForeachArray());
        }
    }

    private long benchIndexArrayList() {
        int counter = 0;
        long start = System.nanoTime();
        int length = arrayList.size();
        for (int i = 0; i < length; i++) {
            if (arrayList.get(i) < HALF_VALUE) {
                ++counter;
            }
        }
        long time = System.nanoTime() - start;
        Log.d("Bench", String.valueOf(counter));
        return time;
    }

    private long benchForeachArrayList() {
        int counter = 0;
        long start = System.nanoTime();
        for (Integer value : arrayList) {
            if (value < HALF_VALUE) {
                ++counter;
            }
        }
        long time = System.nanoTime() - start;
        Log.d("Bench", String.valueOf(counter));
        return time;
    }

    private long benchIndexArray() {
        int counter = 0;
        long start = System.nanoTime();
        int length = array.length;
        for (int i = 0; i < length; i++) {
            if (array[i] < HALF_VALUE) {
                ++counter;
            }
        }
        long time = System.nanoTime() - start;
        Log.d("Bench", String.valueOf(counter));
        return time;
    }

    private long benchForeachArray() {
        int counter = 0;
        long start = System.nanoTime();
        for (Integer value : array) {
            if (value < HALF_VALUE) {
                ++counter;
            }
        }
        long time = System.nanoTime() - start;
        Log.d("Bench", String.valueOf(counter));
        return time;
    }

    private void processResults() {
        // throw away best and worst result
        for (List<Long> result : results) {
            Collections.sort(result);
            result.remove(0);
            result.remove(result.size() - 1);
        }
    }

    private void displayResults() {
        StringBuilder resultBuilder = new StringBuilder("\r\n");

        resultBuilder.append("\r\nArrayList foreach\r\n");
        appendResults(resultBuilder, results.get(FOREACH_ARRAY_LIST));
        resultBuilder.append("\r\n");

        resultBuilder.append("\r\nArrayList index-based\r\n");
        appendResults(resultBuilder, results.get(INDEX_BASED_ARRAY_LIST));
        resultBuilder.append("\r\n");

        resultBuilder.append("\r\nArray foreach\r\n");
        appendResults(resultBuilder, results.get(FOREACH_ARRAY));
        resultBuilder.append("\r\n");

        resultBuilder.append("\r\nArray index-based\r\n");
        appendResults(resultBuilder, results.get(INDEX_BASED_ARRAY));
        resultBuilder.append("\r\n");


        String result = resultBuilder.toString();
        resultView.setText(result);
        Log.i("result", result);
    }


    private void appendResults(StringBuilder builder, List<Long> results) {
        builder.append("best: ")
                .append(String.valueOf(results.get(0) / 1000000.0))
                .append("\r\n")
                .append("worst: ")
                .append("\r\n")
                .append(String.valueOf(results.get(results.size() - 1)  / 1000000.0));

        long sum = 0L;
        for (Long result : results) {
            sum += result;
        }

        double average = sum / (double) results.size();
        builder.append("average: ")
                .append(String.valueOf(average  / 1000000.0))
                .append("\r\n");
    }


    private void setInProgress(boolean isInProgress) {
        buttonRun.setEnabled(!isInProgress);
        progressIndicator.setVisibility(isInProgress ? View.VISIBLE : View.INVISIBLE);
    }
}
